#! /usr/bin/env python3
# coding: utf-8

from src.gui_view import GuiView
from src.gui_model import GuiModel
from src.gui_controller import GuiController
import logging

def main():
  logging.basicConfig(level=logging.DEBUG)

  model = GuiModel()
  view = GuiView()
  controller = GuiController()

  controller.set_model(model)
  controller.set_view(view)
  view.set_controller(controller)
  view.register_to_model(model)

  view.protocol("WM_DELETE_WINDOW", view.on_quit)

  view.mainloop()

if __name__ == "__main__":
  main()