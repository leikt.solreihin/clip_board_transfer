#! /usr/bin/env python3
# coding: utf-8

import argparse
import os
import time

from src.handler import Handler
# from src.mock_handler import MockHandler

#=======================================================================================================================
# PRINT

def print_header():
  print("================================================")
  print("=              Clipboard Transfer              =")
  print("================================================")

def print_network(handler):
  if handler.is_connected: status = "connected to {srv} -> {name}".format(srv=handler.server_address, name=handler.server_name)
  else: status = "not connected"
  print(status)

def print_data(handler):
  # My data
  my_data = handler.client_data
  if my_data is not None:
    print()
    print("\tMy Data : %s" % my_data)
  # All data
  data = handler.data_list
  if len(data) > 0:
    print()
    print("\tData")
    for d in data:
      print("{d}".format(d="\t".join(d)))

def print_menu_network():
  print_header()
  print()
  print("\tConnexion")

def print_menu_main(handler):
  print_header()
  print_network(handler)
  print_data(handler)
  print()
  print("\tMain Menu")
  print("1 - Send")
  print("2 - Sync")

def print_send_menu(handler):
  print_header()
  print_network(handler)
  print_data(handler)
  print()
  print("\tSend new data from {name}".format(name=handler.client_address))

def print_sync_menu(handler):
  print_header()
  print_network(handler)
  print()
  print("\tSynchronizing Data")

#=======================================================================================================================
# LOGIC

def init_handler():
  print("Initialize handler...")
  return Handler()

def menu_network(handler):
  print_menu_network()
  # Get server name
  server_name = None
  while not handler.is_valid_server_name(server_name) and server_name != "":
    server_name = input("Server name ? ")
  # Default server name
  if server_name == "": server_name = "CBT"
  # Try to connect to the server
  if handler.scan(server_name): return menu_main
  # Fail to connect to the server
  print()
  print("\tCan't find a server.")
  ip_address = None
  while not handler.is_valid_ip_address(ip_address) and ip_address != "":
    ip_address = input("Server IP ? (leave empty to create server)\n")
  # Create a new server
  if ip_address == "" and handler.create_server(server_name) and handler.scan(server_name): return menu_main
  # Try to connect to the given ip
  elif handler.connect(ip_address, server_name): return menu_main
  # Fail to connect
  print()
  print("\tFail to connect")
  choice = input("Retry (y/n) ? ")
  if choice.upper() == "Y": return menu_network
  # Quit the program
  return menu_quit

def menu_main(handler):
  print_menu_main(handler)
  choice = input("Your choice (q to quit) : ")
  if choice in ["0", "q", "quit"]: return menu_quit
  if choice == "1": return menu_send
  if choice == "2": return menu_sync
  return menu_main

def menu_quit(handler):
  print_header()
  print()
  print("Quitting...")
  handler.close()
  return None

def menu_send(handler):
  print_send_menu(handler)
  # Get data
  data = input("Data to send (empty to cancel) :\n")
  # Return to main menu if not
  if data == "": return menu_main
  # Send data
  if handler.send_data(data): return menu_main
  # Error
  print()
  input("Fail to send data (enter to continue)")
  return menu_main

def menu_sync(handler):
  print_sync_menu(handler)
  # Try to sync the data
  if handler.sync_data(): return menu_main
  # Error
  print()
  input("Fail to synchronize data (enter to continue)")
  return menu_main

#=======================================================================================================================
# HELPERS

def clear_console():
  os.system("cls") # Clear Windows console

def main():
  # logging.basicConfig(level=logging.DEBUG)
  handler = init_handler()
  variables = {}
  menu = menu_network
  while menu is not None:
    clear_console()
    menu = menu(handler)

#=======================================================================================================================
# CALL MAIN METHOD

if __name__ == "__main__":
  main()
