#! /usr/bin/env python3
# coding: utf-8

import logging
import os
import re
import socket
import threading
import functools

if __name__ != "__main__":
  from src.client import Client
  from src.constants import *

def _ip_list(host_included: bool = False) -> [str]:
  """Retrieve ip addresses from the local netword
  Parameters
  ==========
    host_included: bool (default: False)
  Tell if the device where the code is executed must be in the list.
  Returns
  =======
    list[str]
  list of ip adresses"""

  # Extract ips from arp table
  ips = [s for s in os.popen("arp -a")]
  ips = [re.findall(r"[^\d](\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[^\d]", s) for s in ips]
  ips = functools.reduce(lambda a,b: a+b, filter(([]).__ne__, ips))

  if not host_included: ips.remove(socket.gethostbyname(socket.gethostname()))
  if len(ips) == 0: return []

  logging.debug("Local network ip addresses: {ips}".format(ips=", ".join(ips)))
  return ips

def create_client(port, identification):
  # Retrieve the ips of the network
  ips = _ip_list(True)
  # Find clients that connect to a server
  final_client = None
  for ip in ips:
    client = Client(ip, port, identification)
    if client.connect():
      logging.info("Client connected : %s:%i" % (ip, port))
      final_client = client
      break
  # Return the first client in the list
  return final_client

def find_server(port, identification):
  return create_client(port, identification)

if __name__ == "__main__":
  from client import Client
  from constants import *
  logging.basicConfig(level=logging.DEBUG)

  identification = (SERVER_IDENTIFICATION, input("%s ? " % SERVER_IDENTIFICATION), SERVER_CONFIRMATION)
  c = create_client(PORT, identification)
  if c is not None:
    c.register_event_handler("on_connect", lambda s, n: print("(on_connect) %s:%s" % (s, n)))
    c.register_event_handler("on_disconnect", lambda: print("(on_disconnect)"))
    c.register_event_handler("on_receive", lambda data: print("(on_receive) %s" % data))
    c.register_event_handler("on_send", lambda data: print("(on_send) %s" % data))
    c.register_event_handler("on_server_disconnect", lambda data: print("(on_server_disconnect)"))
    while c.is_connected():
      msg = input()
      c.send(msg)
