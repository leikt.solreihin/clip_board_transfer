#! /usr/bin/env python3
# coding: utf-8

import socket

SERVER_IDENTIFICATION = "leikt_clipboard_transferer:network"

SERVER_CONFIRMATION = "200 OK"

PORT = 1258

SOCKET_FAMILY = socket.AF_INET

SOCKET_TYPE = socket.SOCK_STREAM

END_MESSAGES = ["", "~END~"]

APP_TITLE = "Clip Board Transferer"