#! /usr/bin/env python3
# coding: utf-8

from src.client import Client
import threading
from src.gui_model import GuiModel
import tkinter as tk
import tkinter.ttk as ttk
from tkinter.constants import DISABLED, END, HORIZONTAL, NORMAL
from datetime import datetime

import time
from src.mvc.view import View
import logging

class GuiView(tk.Tk, View):
  def __init__(self):
    tk.Tk.__init__(self)
    View.__init__(self)

    self.title("Clip Board Transferer")
    self.minsize(325, 50)
    self.resizable(True, False)

    window = tk.PanedWindow(self, orient=tk.VERTICAL, width=300)
    window.pack(padx=2, pady=2, fill=tk.BOTH, side=tk.TOP)

    self.server_entry = ServerEntry(window)
    window.add(self.server_entry)

    window.add(ttk.Separator(window, orient=tk.HORIZONTAL), pady=5)

    self.client_gui = MainClientGUI(window)
    window.add(self.client_gui)

    window.add(ttk.Separator(window, orient=tk.HORIZONTAL), pady=5)

    self.data_gui = DataGUI(window)
    window.add(self.data_gui)

  def set_controller(self, controller):
    self.server_entry.set_controller(controller)
    self.client_gui.set_controller(controller)
    self.data_gui.set_controller(controller)
    View.set_controller(self, controller)

  def register_to_model(self, model):
    View.register_to_model(self, model)
    self.client_gui.register_to_model(model)
    self.data_gui.register_to_model(model)
    self.server_entry.register_to_model(model)

  def display_error(self, message):
    logging.error(message)

  def on_quit(self):
    self._controller.on_quit()
    self.destroy()
    quit()

class ServerEntry(tk.Frame, View):
  def __init__(self, master):
    super().__init__(master)

    # First line
    window = ttk.PanedWindow(self, orient=tk.HORIZONTAL)
    window.pack(fill=tk.X)

    window.add(tk.Label(window, text="Server's name : ", width=15, anchor=tk.W))

    self.server_name_entry = tk.Entry(window)
    self.server_name_entry.insert(0, "cbt")
    self.server_name_entry.bind("<Return>", self.on_click_scan_button)
    window.add(self.server_name_entry)

    self.scan_buttton = tk.Button(window, text="Scan", width=7, command=self.on_click_scan_button)
    window.add(self.scan_buttton)

    # Second line 
    window = ttk.PanedWindow(self, orient=tk.HORIZONTAL)
    window.pack(fill=tk.X)

    window.add(tk.Label(window, text="Server's IP : ", width=15, anchor=tk.W))

    self.server_ip_entry = tk.Entry(window)
    self.server_ip_entry.bind("<Return>", self.on_click_search_button)
    window.add(self.server_ip_entry)
    
    self.connect_button = tk.Button(window, text="Connect", width=7, command=self.on_click_search_button)
    window.add(self.connect_button)

    # Third line
    self.ip_label = tk.Label(self)
    self.ip_label.pack(side=tk.LEFT)

    self.status_label = tk.Label(self, text="not connected")
    self.status_label.pack(side=tk.RIGHT)

  def on_click_scan_button(self, *args):
    server_name = self.server_name_entry.get()
    self._controller.on_scan(server_name)

  def on_click_search_button(self, *args):
    server_name = self.server_name_entry.get()
    server_ip = self.server_ip_entry.get()
    self._controller.on_connect(server_name, server_ip)

  def on_model_update(self, model, code: str):
    if code == GuiModel.CONNEXION_SUCCESS:
      self.server_name_entry.config(state=DISABLED)
      self.server_ip_entry.config(state=DISABLED)
      self.ip_label.config(text=model.server_address)
      self.status_label.config(text="connected")

    elif code == GuiModel.FAIL_SCAN:
      self.ip_label.config(text="Scan has failed...")
      self.status_label.config(text="not connected")

    elif code == GuiModel.FAIL_CONNEXION:
      self.ip_label.config(text="Connexion with IP has failed...")
      self.status_label.config(text="not connected")

    elif code == GuiModel.SERVER_CREATED:
      self.ip_label.config(text=model.server_address)
      self.status_label.config(text="host")

class ClientGUI(tk.Frame, View):
  def __init__(self, master, name = "unkown") -> None:
    super().__init__(master)
    self.name = name
    self.binded = False

    self.status_gui = StatusGUI(self)
    self.status_gui.pack(fill=tk.X)
    self.status_gui.set_ip(name, "")
    self.status_gui.set_status("not connected")

    self.textarea = tk.Text(self, height=5)
    self.textarea.pack(fill=tk.X)
    # self.textarea.bind("<<Modified>>", self.on_modification)

  def uneditable(self):
    self.textarea.config(state=DISABLED, background='grey80')

  def on_modification(self, event):
    self.status_gui.set_message("")
    self.textarea.edit_modified(False)

  def on_model_update(self, model, code: str):
    if code == GuiModel.DATA_SYNC:
      msg = model.my_data(self.name)
      if len(msg) == 0: return # No message
      # Update text area
      self.textarea.config(state=NORMAL)
      self.textarea.delete(1.0, END)
      self.textarea.insert(tk.INSERT, msg[1])
      self.textarea.config(state=DISABLED)
      # Update status
      t = datetime.now().strftime("%H:%M:%S")
      self.status_gui.set_message("(updated: %s)" % t)
      self.status_gui.set_ip(self.name, "")
      self.status_gui.set_status("connected")

class MainClientGUI(ClientGUI, View):
  def __init__(self, master) -> None:
      super().__init__(master)
      self.send_button = tk.Button(self, text="Send", command=self.on_send)
      self.send_button.pack(pady=5, fill=tk.X)
  
      self.binded = True
    
  def uneditable(self):
    pass # Never uneditable

  def on_model_update(self, model: GuiModel, code: str):
    if code == GuiModel.CONNEXION_SUCCESS or code == GuiModel.SERVER_CREATED:
      self.status_gui.set_ip(model.name, "ME")
      self.status_gui.set_status("connected")
    elif code == GuiModel.DATA_SEND:
      t = datetime.now().strftime("%H:%M:%S")
      self.status_gui.set_message("(sent at %s)" % t)

  def on_send(self):
    message = self.textarea.get("1.0", END)
    self._controller.on_send(message)

class ClientGUIWithCopyButton(ClientGUI, View):
  def __init__(self, master, name = "unkown") -> None:
    super().__init__(master)
    master.add(tk.Button(master, text="Copy to clipboard", command=self.on_copy_button))

  def on_copy_button(self):
    msg = self.textarea.get("1.0", END)
    self._controller.on_copy(msg)

class StatusGUI(tk.Frame, View):
  """Class that display the server status gui"""

  def __init__(self, master) -> None:
    super().__init__(master)
    self._ip_label = tk.Label(self, text=self.status_text("unknown", "unkown"))
    self._ip_label.pack(side=tk.LEFT)

    self._status_label = tk.Label(self, text="not connected")
    self._status_label.pack(side=tk.RIGHT)

    self._message_label = tk.Label(self)
    self._message_label.pack(side=tk.RIGHT)

  def status_text(self, ip, name):
    return "%s (%s)" % (ip, name)

  def set_ip(self, ip: str, name: str):
    self._ip_label.config(text=self.status_text(ip, name))

  def set_message(self, message):
    self._message_label.config(text=message)

  def set_status(self, value):
    self._status_label.config(text=value)

class DataGUI(tk.Frame, View):
  def __init__(self, master):
    super().__init__(master)

    self._clients = []
    self._clients_names = []

    master.add(tk.Label(master))
    # master.add(tk.Label(master, text="Network data", anchor=tk.CENTER))
    master.add(tk.Button(master, text="Synchronize", command=self.on_sync))

    for i in range(3):
      client_gui = ClientGUIWithCopyButton(master)
      client_gui.uneditable()
      master.add(client_gui)
      self._clients.append(client_gui)
    
  def set_controller(self, controller):
    View.set_controller(self, controller)
    for client_gui in self._clients:
      client_gui.set_controller(controller)

  def register_to_model(self, model):
    View.register_to_model(self, model)
    for client_gui in self._clients:
      client_gui.register_to_model(model)

  def on_sync(self):
    self._controller.on_sync()

  def on_model_update(self, model: GuiModel, code: str):
    if code == GuiModel.DATA_SYNC:
      self.update_clients(model, code)

  def update_clients(self, model: GuiModel, code: str):
    logging.info(model.data_list)
    for name, _value in model.data_list:
      if name == model.name: continue

      if not name in self._clients_names:
        for client in self._clients:
          if client.binded: continue

          client.name = name
          client.binded = True
          client.on_model_update(model, code)
          self._clients_names.append(name)
          break


      
