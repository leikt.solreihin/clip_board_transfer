import time
import re

class MockHandler():
  def scan(self, name: str) -> bool:
    time.sleep(2)
    return False

  def create_server(self, name: str) -> bool:
    time.sleep(2)
    return False

  def connect(self, host: str, name: str) -> bool:
    time.sleep(2)
    return False

  def set_data(self, data: str):
    time.sleep(2)
    return False

  def sync_data(self):
    time.sleep(2)
    return False

  def is_valid_server_name(self, server_name):
    return server_name != None

  def is_valid_ip_address(self, ip_address):
    return ip_address != None and re.match(r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", ip_address)

  def close(self):
    return True

  @property
  def data_list(self):
    return [["000.000.0.0:00001", "its clipboard"], ["000.000.0.0:00002", "Lucifer clipboard"], ["000.000.0.0:00003", "666 forever"], ["666.666.6.6:12345", "my clip board"]]

  @property
  def client_data(self):
    return ["666.666.6.6:12345", "my clip board"]

  @property
  def is_connected(self):
    return True

  @property
  def server_address(self):
    return ("123.123.0.0", "12345")

  @property
  def client_address(self):
    return ("333.333.0.0", "12345")

  @property
  def server_name(self):
    return "CBT"