#! /usr/bin/env python3
# coding: utf-8

import logging
import socket
import threading

class Client():
  TIMEOUT = 1.0

  def __init__(self, host: str, port: int, identification: tuple):
    # Address
    self._host = host
    self._port = port
    # Identitifaction (question, answer)
    self._identification = tuple([s.encode("Utf8") for s in identification])
    # Init socket
    self._socket = None
    # Threading
    self._lock = threading.Lock()
    # State
    self._connected = False
    # Event proc
    self._events = {
      "on_connect": [],           # When the client connect to the server successfully
      "on_disconnect": [],        # When the client disconnect from the server
      "on_receive": [],           # When the client receive data
      "on_send": [],              # When the client send data
      "on_server_disconnect": []  # WHen the server disconnect from the client
    }

  def is_connected(self) -> bool:
    """Return the state of the connexion (True: connected, False: not connected)"""
    return self._connected

  def connect(self) -> bool:
    """Try to connect to the server, return True if the connexion is successful"""
    try:
      # Init socket
      server_name = "%s:%i" % (self._host, self._port)
      self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self._socket.settimeout(self.TIMEOUT)
      self._socket.connect((self._host, self._port))

      # Identification (should it clse the socket ?)
      if self._socket.recv(1024) != self._identification[0]: raise ConnectionError("Cannot connect to %s" % server_name)
      self._socket.send(self._identification[1])
      if self._socket.recv(1024) != self._identification[2]: raise ConnectionError("Cannot connect to %s (%s)" % (server_name, self._identification[1]))
      self._connected = True
      self._trigger_event("on_connect", server_name, self._identification[1].decode("Utf8"))

      # Init reception
      self._socket.settimeout(None)
      self._recv_thread = threading.Thread(target = self._recv, daemon=True)
      self._recv_thread.start()
      return True
    except socket.error as err:
      logging.info(err)
      return False
    except Exception as err:
      logging.error(err)
      return False

  def disconnect(self, is_server_order = False):
    """Disconnect the client from the server"""

    if not self.is_connected(): return
    try:
      self._trigger_event("on_server_disconnect") if is_server_order else self._trigger_event("on_disconnect")
      self._socket.send("".encode("Utf8"))
      self._socket.close()
      try: self._recv_thread.join()
      except: pass
    except socket.error as err:
      logging.error(err)
    finally:
      self._connected = False

  def send(self, data: str):
    """Send the given data to the server"""
    if data is None or data == "": return self.disconnect()
    
    try:
      self._trigger_event("on_send", data)
      self._socket.send(data.encode("Utf8"))
    except socket.error as err:
      logging.error(err)

  def register_event_handler(self, event: str, handler: callable) -> bool:
    """Register a callable that will be triggered when the evens triggers"""

    if not event in self._events: return False
    if handler in self._events[event]: return False

    self._events[event].append(handler)
    return True

  @property
  def server_address(self):
    return (self._host, self._port)

  @property
  def client_address(self):
    return self._socket.getsockname()

  #===================
  # Private functions

  def _trigger_event(self, event, *args):
    if not event in self._events: return

    self._lock.acquire()
    for handler in self._events[event]:
      try:
        handler(*args)
      except Exception as err:
        logging.warning(err)
    self._lock.release()

  def _recv(self):
    while 1:
      try:
        data = self._socket.recv(1024).decode("Utf8")
        if data == "" or data == "STOP": return self.disconnect(True)
        self._trigger_event("on_receive", data)
      except socket.error: return
    
if __name__ == "__main__":
  from constants import PORT, SERVER_IDENTIFICATION, SERVER_CONFIRMATION
  logging.basicConfig(level=logging.DEBUG)

  identification = (SERVER_IDENTIFICATION, input("%s ? " % SERVER_IDENTIFICATION), SERVER_CONFIRMATION)
  c = Client("192.168.1.19", PORT, identification)
  c.register_event_handler("on_connect", lambda s, n: print("(on_connect) %s:%s" % (s, n)))
  c.register_event_handler("on_disconnect", lambda: print("(on_disconnect)"))
  c.register_event_handler("on_receive", lambda data: print("(on_receive) %s" % data))
  c.register_event_handler("on_send", lambda data: print("(on_send) %s" % data))
  c.register_event_handler("on_server_disconnect", lambda: print("(on_server_disconnect)"))
  c.connect()
  while c.is_connected():
    msg = input()
    c.send(msg)