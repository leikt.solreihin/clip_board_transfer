#! /usr/bin/env python3
# coding: utf-8

import json
import logging
from src.mvc.model import Model
from src.handler import Handler

class GuiModel(Model):
  """Model that manage all the data and connexion"""

  # CONNEXION
  CONNEXION_SUCCESS = 1000
  SERVER_CREATED = 1001
  # DATA
  DATA_SEND = 1100
  DATA_SYNC = 1101
  # ERRORS
  FAIL_SCAN = 2000
  FAIL_CONNEXION = 2001
  CLIENT_NOT_CONNECTED = 2002

  def __init__(self) -> None:
      super().__init__()
      self._is_connected = False
      self._handler = Handler()

  @property
  def is_connected(self):
    return self._is_connected

  @property
  def server_address(self):
    return ":".join([str(s) for s in self._handler.server_address])

  @property
  def name(self):
    return self._handler.client_address[0]

  def my_data(self, name):
    data = list(filter(lambda x: x[0] == name, self._handler.data_list))
    if len(data) > 0: return data[0]
    return []

  @property
  def data_list(self):
    return self._handler.data_list

  def connect_via_scan(self, server_name: str):
    if self.is_connected: return
    # Scan
    self._is_connected = self._handler.scan(server_name)
    # Notify observers
    if self.is_connected: self.notify_observers(self.CONNEXION_SUCCESS)
    else: self.notify_observers(self.FAIL_SCAN)

  def connect_via_ip(self, server_name: str, server_ip: str):
    if self.is_connected: return
    # Status
    self._is_connected = self._handler.connect(server_ip, server_name)
    # Notify observers
    if self.is_connected: self.notify_observers(self.CONNEXION_SUCCESS)
    else: self.create_server(server_name)

  def send(self, message: str):
    if not self.is_connected: return self.notify_observers(self.CLIENT_NOT_CONNECTED)
    # Send
    # data = json.dumps({"origin": self.name, "operation": "SET", "value": message})
    self._handler.send_data(message)
    # Notify observers
    self.notify_observers(self.DATA_SEND)

  def sync(self):
    # Sync
    self._handler.sync_data()

    # Notify observers
    self.notify_observers(self.DATA_SYNC)

  def disconnect(self):
    self._handler.close()

  #=====================================================================
  # PRIVATE

  def create_server(self, server_name):
    if self.is_connected: return
    # Status
    self._is_connected = self._handler.create_server(server_name) and self._handler.scan(server_name)
    # Notify observers
    if self.is_connected: self.notify_observers(self.SERVER_CREATED)
    else: self.notify_observers(self.FAIL_CONNEXION)