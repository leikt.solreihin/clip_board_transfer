#! /usr/bin/env python3
# coding: utf-8

class Model():
  def __init__(self) -> None:
    self._observers = []

  def register_observer(self, observer):
    if observer not in self._observers: self._observers.append(observer)

  def notify_observers(self, code: str = ''):
    for observer in self._observers:
      observer.on_model_update(self, code)