#! /usr/bin/env python3
# coding: utf-8

import tkinter as tk

class View():
  def __init__(self):
    self._controller = None

  def set_controller(self, controller):
    self._controller = controller

  def register_to_model(self, model):
    model.register_observer(self)

  def on_model_update(self, model, code: str):
    pass