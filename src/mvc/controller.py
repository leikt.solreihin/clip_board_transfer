#! /usr/bin/env python3
# coding: utf-8

class Controller():
  def __init__(self) -> None:
      self._model = None
      self._view = None

  def set_model(self, model):
    self._model = model

  def set_view(self, view):
    self._view = view

  def on_model_update(self, model, code: str):
    pass
