#! /usr/bin/env python3
# coding: utf-8

import logging
import socket
import threading


class Server():
  MAX_CONNEXIONS = 5

  def __init__(self, port: int, identitication: tuple):
    # server address
    self._host = ""
    self._port = port
    # (question, answer, confirmation message)
    self._identitication = tuple([s.encode("Utf8") for s in identitication])
    # server socket
    self._socket = None
    # { client_address => socket}
    self._clients = {}
    # Threading
    self._lock = threading.Lock()
    self._threads = []
    # Events
    self._events = {
      "on_start": [],
      "on_stop": [],
      "on_wait": [],
      "on_receive": [],
      "on_client_connect": [],
      "on_client_disconnect": []
    }


  def start(self):
    """Start the server. Return True if the server started correctly"""
    try:
      # Retrieve the host name
      self._host = self._retrieve_local_ip()
      # Creating the socket
      self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self._socket.bind(self.address())
      # Start listening
      self._socket.listen(self.MAX_CONNEXIONS)
      # Start connexion thread that will die with the program (daemon)
      thread = threading.Thread(target = self._accept_clients)
      thread.start()
      self._threads.append(thread)
      self._trigger_event("on_start", self._host, self._port)
      return True
    except Exception as err:
      logging.error(err)
      return False


  def address(self):
    """Return the (ip, port) tuple"""
    return (self._host, self._port)


  def name(self):
    """Return the ip:port string"""
    return "%s:%i" % self.address()


  def stop(self):
    """Stop the server"""
    # Disconnect every client
    for client_name, connexion in list(self._clients.items()):
      self._disconnect_client(client_name, connexion)
    # Shutdown the socket
    self._socket.close()

    # Wait fot the threads to finish
    for thread in self._threads:
      thread.join()
    # Trigger the event
    self._trigger_event("on_stop")


  def register_event_handler(self, event: str, handler: callable) -> bool:
    """Register a callable that will be triggered when the event triggers"""

    if not event in self._events: return False
    if handler in self._events[event]: return False

    self._events[event].append(handler)
    return True
      


  #===================
  # Private functions

  def _retrieve_local_ip(self):
    return socket.gethostbyname(socket.gethostname())


  def _accept_clients(self):
    while 1: # That thread should be daemon and unique
      self._trigger_event("on_wait")
      # Accept new connexion
      try: connexion, client_adress = self._socket.accept()
      except: break
      # Check question and answer
      if not self._identification_procedure(connexion): continue
      client_name = "%s:%i" % client_adress
      self._lock.acquire()
      # Add the client to the connexions
      self._clients[client_name] = connexion
      # Start a listening thread
      thread = threading.Thread(target = self._listen_client, args = (client_name,), daemon=True)
      self._threads.append(thread)
      self._lock.release()

      thread.start()
      self._trigger_event("on_client_connect", client_adress[0], client_adress[1])


  def _identification_procedure(self, connexion) -> bool:
    try:
      connexion.send(self._identitication[0]) # Question
      if connexion.recv(1024) != self._identitication[1]: # Wrong Answer
        connexion.close()
        return False
      connexion.send(self._identitication[2]) # Confirm identification
      return True
    except socket.error:
      return False


  def _listen_client(self, name: str):
    # Security
    self._lock.acquire()
    if name in self._clients: connexion = self._clients[name]
    self._lock.release()
    if connexion is None: return

    while 1: # That thread should be daemon
      try:
        data = connexion.recv(1024).decode("Utf8")
        if data == "": return self._disconnect_client(name, connexion)
        if data == "STOP": return self.stop()

        self._trigger_event("on_receive", name, data)
        self._sendall(data)

      except socket.error as err:
        self._disconnect_client(name, connexion)
        break
    

  def _disconnect_client(self, name, connexion):
    try: connexion.send("DISCONNECT".encode("Utf8"))
    except: pass
    try:connexion.close()
    except: pass

    self._lock.acquire()
    try: del self._clients[name]
    except: pass
    self._lock.release()
    self._trigger_event("on_client_disconnect", name)


  def _sendall(self, data, excluded = []):
    data = data.encode("Utf8")

    for name in self._clients:
      if name in excluded: continue
      self._clients[name].send(data)


  def _trigger_event(self, event, *args):
    if not event in self._events: return

    self._lock.acquire()
    for handler in self._events[event]:
      try:
        handler(*args)
      except Exception as err:
        logging.warning(err)
    self._lock.release()


if __name__ == "__main__":
  from constants import *
  logging.basicConfig(level=logging.DEBUG)
  identification = (SERVER_IDENTIFICATION, input("%s ? " % SERVER_IDENTIFICATION), SERVER_CONFIRMATION)
  s = Server(PORT, identification)
  s.register_event_handler("on_start", lambda h,p: print("(on_start) %s:%s" % (h,p)))
  s.register_event_handler("on_wait", lambda: print("(on_wait)"))
  s.register_event_handler("on_client_connect", lambda a,p: print("(on_client_connect) %s:%s" % (a,p)))
  s.register_event_handler("on_receive", lambda n,s: print("(on_receive) from %s : %s" % (n,s)))
  s.register_event_handler("on_client_disconnect", lambda n: print("(on_client_disconnect) %s" % n))
  s.register_event_handler("on_stop", lambda: print("(on_stop)"))
  s.start()
  # s.stop()