import logging
import time
import re
import json
import threading

from src.scan import find_server
from src.server import Server
from src.client import Client

class Handler():
  """Backend data and connexion handler"""

  #=======================================================================================================================
  # PUBLIC

  def __init__(self):
    self._client = None
    self._server = None
    self._local_data = {}
    self._network_name = "*unkown*"
    self._lock = threading.Lock()

  def scan(self, name: str) -> bool:
    """Scan the local network and connect the client to a server if it exists

    Args:
        name (str): name of the server

    Returns:
        bool: True if the client is connected to a server
    """
    client = find_server(self._port(), self._identification(name))
    # Wall the not connected client
    if client is None or not client.is_connected(): return False
    # Assign client and network name
    self._client = client
    self._network_name = name
    self._client.register_event_handler("on_receive", self._on_recv)
    return True

  def connect(self, host: str, name: str) -> bool:
    """Connect the client to the host

    Args:
        host (str): IP address of the server
        name (str): name of the server

    Returns:
        bool: True if the client is connected to the server
    """
    client = Client(host, self._port(), self._identification(name))
    client.connect()
    # Wall the not connected client
    if client is None or not client.is_connected(): return False
    # Assign client and network name
    self._client = client
    self._client.register_event_handler("on_receive", self._on_recv)
    self._network_name = name
    return True

  def create_server(self, name: str) -> bool:
    """Create a server on this device

    Args:
        name (str): name of the server

    Returns:
        bool: True if the server is created
    """
    self._server = Server(self._port(), self._identification(name))
    self._server.start()
    self._network_name = name
    return True

  def send_data(self, data: str):
    """Update the data of this client

    Args:
        data (str): new data
    """
    request = json.dumps({
      "operation": "SET",
      "origin": self.client_address[0],
      "value": data
    })
    self._client.send(request)
    return True
  
  def sync_data(self):
    """Synchronize data from the server
    """
    self._lock.acquire()
    self._lock.release()
    return True

  def is_valid_server_name(self, server_name: str) -> bool:
    """Check if the given name is a valid server name

    Args:
        server_name (str): server name

    Returns:
        bool: True if the given name is valid
    """
    return server_name != None

  def is_valid_ip_address(self, ip_address: str) -> bool:
    """Check if the given ip is valid

    Args:
        ip_address (str): ip to test

    Returns:
        bool: True if the given ip is valid
    """
    return ip_address != None and re.match(r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", ip_address)

  def close(self):
    """Close the connexion and server
    """
    if self._server is not None: self._server.stop()
    return True

  @property
  def data_list(self) -> list[tuple]:
    return list(self._local_data.items())

  @property
  def client_data(self):
    if self._client is not None: return self._local_data.get(self._client.client_address[0], None)
    return None

  @property
  def is_connected(self):
    return self._client is not None and self._client.is_connected()

  @property
  def server_address(self):
    if self._client is not None: return self._client.server_address
    return "*unknown"

  @property
  def client_address(self):
    if self._client is not None: return self._client.client_address
    return ("unkown", "unkwon")

  @property
  def server_name(self):
    return self._network_name

    
  #=======================================================================================================================
  # PROTECTED

  _PORT = 1258
  _QUESTION = "CBT?"
  _CONFIRMATION = "CBT_CONNECTED"

  def _port(self) -> int:
    """Return the port where the server connect"""
    return self._PORT

  def _identification(self, name: str) -> tuple:
    """Return a tuple of server identification"""
    return (self._QUESTION, name, self._CONFIRMATION)

  def _on_recv(self, data: str):
    data = json.loads(data)
    self._lock.acquire()
    logging.info(data["origin"])
    if data["operation"] == "SET": self._local_data[data["origin"]] = data["value"]
    self._lock.release()