#! /usr/bin/env python3
# coding: utf-8

import logging
import subprocess
import tkinter as tk

from src.mvc.controller import Controller

class GuiController(Controller):
  """Control the view and model"""

  def on_scan(self, server_name: str):
    if self._model.is_connected:
      self._view.display_error("Client already connected")
      return

    logging.info("Ask the model for a scan...")
    self._model.connect_via_scan(server_name)

  def on_connect(self, server_name: str, server_ip: str):
    if self._model.is_connected:
      self._view.display_error("Client already connected")
      return
    
    logging.info("Ask the model to connect to %s:%s" % (server_ip, server_name))
    self._model.connect_via_ip(server_name, server_ip)

  def on_send(self, message: str):
    if not self._model.is_connected:
      self._view.display_error("Client not connected")
      return 
    
    logging.info("Ask the model to send a message")
    self._model.send(message)

  def on_sync(self):
    if not self._model.is_connected:
      self._view.display_error("Client not connected")
      return

    logging.info("Ask the model to sync the data...")
    self._model.sync()

  def on_copy(self, msg: str):
    logging.info("Copying to clipboard: %s" % msg)
    r = tk.Tk()
    r.clipboard_clear()
    r.clipboard_append(msg)
    r.destroy()


  def on_quit(self):
    if not self._model.is_connected: return

    self._model.disconnect()